<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable{
	
	use SoftDeletes, Notifiable;
	
	protected $hidden = [
		'password', 'remember_token',
	];
	
	protected $fillable = ['first_name', 'middle_name', 'last_name', 'profession', 'email'];
	
	public function getFullNameAttribute(){
		return $this->first_name.' '.$this->middle_name.' '.$this->last_name;
	}
	
	public function getPhotoAttribute(){
		if(empty($this->photo)){
			return asset('img/avatar.png');
		}
		
		return asset('storage/avatar/'.$this->photo);
	}
	
	public function roles(){
		return $this->belongsToMany('App\Role', 'user_roles');
	}
}
