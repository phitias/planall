<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;
		
		const ROLE_SUPERADMIN = 8;
		const ROLE_SUBADMIN = 9;
    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
		
		public function reset(Request $request){
			$this->validate($request, $this->rules(), $this->validationErrorMessages());
			$response = $this->broker()->reset(
				$this->credentials($request), function ($user, $password) {
					$this->resetPassword($user, $password);
				}
			);
				
			$user = User::where('email', $request->email)->first();
			$userRoleIds = $user->roles->pluck('id')->toArray();
			
			if(in_array(SELF::ROLE_SUPERADMIN, $userRoleIds) 
				|| in_array(SELF::ROLE_SUBADMIN, $userRoleIds)){
				$this->redirectTo = '/dashboard';
			}
			
			return $response == Password::PASSWORD_RESET
				? $this->sendResetResponse($response)
				: $this->sendResetFailedResponse($request, $response);
		}
}
