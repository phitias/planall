<?php

namespace App\Http\Controllers\Api\v1;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller{
	
    public function index(){
			$i = 0;
			$data = [];
      $users =  User::withTrashed()->get();
			
			foreach($users as $user){
				$data[$i] = [
					'id' => $user->id,
					'first_name' => $user->first_name,
					'middle_name' => ($user->middle_name)?$user->middle_name:'',
					'last_name' => ($user->last_name)?$user->last_name:'',
					'profession' => ($user->profession)?$user->profession:'',
					'photo' => $user->photo,
					'email' => $user->email,
					'roles' => $user->roles->pluck('name'),
					'roleIds' => $user->roles->pluck('id'),
					'status' => !empty($user->deleted_at)?'trashed' : $user->status
				];
				$i++;
			}
			
			return $data;
    }

    public function store(Request $request){
			$data = $request->except(['id']);
      $user = User::create($data);
			if($user){
				if($user->roles()->sync($request->roles)){
					return ['status' => 'success'];
				}
			}
			return ['status' => 'failed'];
    }
		
    public function update(Request $request, $id){
			$data = $request->except(['id', 'roles']);
			$user = User::withTrashed()->where('id', $id)->first();
      if($user->update($data)){
				if($user->roles()->sync($request->roles)){
					return ['status' => 'success'];
				}
			}
			return ['status' => 'failed'];
    }

   /*  public function block($id){
      if(User::withTrashed()->where('id', $id)->first()->update(['status' => 'block'])){
				return ['status' => 'success'];
			}
			return ['status' => 'failed'];
    }
		
    public function unblock($id){
      if(User::withTrashed()->where('id', $id)->first()->update(['status' => 'active'])){
				return ['status' => 'success'];
			}
			return ['status' => 'failed'];
    } */
		
    public function trash($id){
      if(User::findOrFail($id)->delete()){
				return ['status' => 'success'];
			}
			return ['status' => 'failed'];
    }
		
    public function restore($id){
      if(User::withTrashed()->where('id', $id)->restore()){
				return ['status' => 'success'];
			}
			return ['status' => 'failed'];
    }
		
    public function destroy($id){
			$user = User::withTrashed()->where('id', $id)->first();
			
      if($user->roles()->detach()){
				if($user->forceDelete()){
					return ['status' => 'success'];
				}
			}
			return ['status' => 'failed'];
    }
		
}
