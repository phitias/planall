import Vue from 'vue'
import Router from 'vue-router'

import dashboardIndex from './pages/dashboard/index.vue'
import dashboardUsers from './pages/dashboard/users/index.vue'
import dashboardUserRoles from './pages/dashboard/users/index.roles.vue'

Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      name: 'dashboard-index',
      component: dashboardIndex
    },
    {
      path: '/users',
      name: 'dashboard-users',
      component: dashboardUsers
    },
    {
      path: '/user/roles',
      name: 'dashboard-user-roles',
      component: dashboardUserRoles
    }
	]
})