<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1', 'namespace' => 'Api\v1', 'as' => 'api.'], function () {
    Route::resource('user', 'UserController', ['except' => ['edit']]);
		Route::put('user/block/{user}', 'UserController@block')->name('user.block');
		Route::put('user/unblock/{user}', 'UserController@unblock')->name('user.unblock');
		Route::put('user/trash/{user}', 'UserController@trash')->name('user.trash');
		Route::put('user/restore/{user}', 'UserController@restore')->name('user.restore');
		
    Route::resource('role', 'RoleController', ['except' => ['edit']]);
		Route::put('role/trash/{role}', 'RoleController@trash')->name('role.trash');
		Route::put('role/restore/{role}', 'RoleController@restore')->name('role.restore');
		
});

