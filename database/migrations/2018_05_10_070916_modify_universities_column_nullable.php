<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUniversitiesColumnNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('universities', function($table) {
				$table->string('website', 255)->nullable()->change();
				$table->string('facebook', 255)->nullable()->change();
				$table->string('twitter', 255)->nullable()->change();
				$table->string('youtube', 255)->nullable()->change();
				$table->string('linkedin', 255)->nullable()->change();
				$table->text('map_address')->nullable()->change();
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
