<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUniversityAlumnisColumnNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('university_alumnis', function($table) {
				$table->string('famous_name2', 100)->nullable()->change();
				$table->string('famous_photo2', 100)->nullable()->change();
				$table->string('famous_name3', 100)->nullable()->change();
				$table->string('famous_photo3', 100)->nullable()->change();
				$table->string('famous_name4', 100)->nullable()->change();
				$table->string('famous_photo4', 100)->nullable()->change();
				$table->string('famous_name5', 100)->nullable()->change();
				$table->string('famous_photo5', 100)->nullable()->change();
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
