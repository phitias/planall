<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
						$table->integer('user_id');
						$table->integer('category_id');
						$table->string('title');
						$table->string('hero_image', 255);
						$table->integer('viewed');
						$table->integer('liked');
						$table->integer('disliked');
						$table->enum('type', ['blog', 'forum']);
						$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
