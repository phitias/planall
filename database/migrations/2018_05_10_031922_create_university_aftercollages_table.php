<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversityAftercollagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_aftercollages', function (Blueprint $table) {
            $table->increments('id');
						$table->integer('university_id');
						$table->text('earning');
						$table->tinyInteger('placement_rate');
						$table->text('top_employee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('university_aftercollages');
    }
}
