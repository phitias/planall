<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
						$table->string('title', 150);
						$table->text('description');
						$table->date('start_date');
						$table->date('end_date');
						$table->time('start_time');
						$table->time('end_time');
						$table->text('media');
						$table->string('hero_image', 255);
						$table->enum('status', ['active', 'done', 'canceled', 'pending'])->default('pending');
						$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
