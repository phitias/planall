<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversityProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_programs', function (Blueprint $table) {
            $table->increments('id');
						$table->integer('university_id');
						$table->integer('category_id');
						$table->string('undergraduate', 20);
						$table->string('MS', 20);
						$table->string('MBA', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('university_programs');
    }
}
