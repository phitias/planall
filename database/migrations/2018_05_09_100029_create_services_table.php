<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
						$table->string('name', 100);
						$table->text('description');
						$table->string('country', 255);
						$table->string('city', 255);
						$table->string('address', 255);
						$table->text('map_address');
						$table->string('contact_phone', 20);
						$table->string('contact_name', 100);
						$table->year('since');
						$table->string('website', 100);
						$table->time('open_at');
						$table->time('closed_at');
						$table->tinyInteger('home_delivery')->default(0);
						$table->integer('viewed');
						$table->string('icon', 255);
						$table->string('logo', 255);
						$table->string('hero_image', 255);
						$table->string('facebook', 100);
						$table->string('twitter', 100);
						$table->string('youtube', 100);
						$table->string('linkedin', 100);
						$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
