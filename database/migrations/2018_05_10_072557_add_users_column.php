<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('users', function($table) {
				$table->string('photo', 150)->nullable();
				$table->string('hero_image', 150)->nullable();
				$table->string('profession', 150)->nullable();
				$table->date('birth_day')->nullable();
				$table->string('country', 150)->nullable();
				$table->string('number', 15)->nullable();
				$table->text('interest_course')->nullable();
				$table->text('interest_location')->nullable();
				$table->integer('dream_university')->nullable();
				$table->string('facebook', 255)->nullable();
				$table->string('twitter', 255)->nullable();
				$table->string('instagram', 255)->nullable();
				$table->string('linkedin', 255)->nullable();
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
